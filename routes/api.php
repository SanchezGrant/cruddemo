<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResources(['user'=>'API\UserController']);
Route::get('findParticiapants','API\ParticipantsController@search');
Route::get('getCounts','API\ParticipantsController@countTotal');
Route::get('updateAttendance/{participants}','API\ParticipantsController@updateAtt');
Route::get('updatePayment/{participants}','API\ParticipantsController@updatePay');
Route::get('updateAttendancePayment/{participants}','API\ParticipantsController@updateAttPay');
Route::apiResources(['participants'=>'API\ParticipantsController']);
Route::apiResources(['chapels'=>'API\ChapelsController']);