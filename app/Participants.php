<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Chapels;

class Participants extends Model
{
    // protected $table = 'participants';
    // protected $primary = 'id';
    protected $fillable = [
        'first_name', 'last_name', 'chapel_name','payment','attendance','remarks',
    ];

    public function chapels(){
        return $this->belongsTo('App\Chapels');
    }
}
