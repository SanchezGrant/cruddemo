<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Participants;
use App\Chapels;
use Illuminate\Support\Str;

class ParticipantsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return Participants::latest()->paginate(20);
        // return Participants::orderBy('payment', 'DESC')->paginate(20);
        // 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'first_name' => 'required|string|max:191',
            'last_name' =>  'required|string|max:191',

        ]);


        return  Participants::create([
            'first_name' => Str::ucfirst($request['first_name']),
            'last_name' => Str::ucfirst($request['last_name']),
            'chapel_name' => $request['chapel_name'],
            'payment' => $request['payment'],
            'attendance' => $request['attendance'],
            'remarks'=>$request['remarks'],
        ]);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $participant = Participants::findOrFail($id);

        $this->validate($request,[
            'first_name' => 'required|string|max:191',
            'last_name' =>  'required|string|max:191',
           
        ]);
            

        $participant->update($request->all());

        return ['message'=>'updated user info'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $array = explode(',', $id);
        foreach ($array as $idTodelete){      
            $user = Participants::findOrFail($idTodelete);
            $user->delete();
        }
    }

    public function search(){
        
        if($search = \Request::get('q')){
            $users = Participants::where(function($query) use ($search){
                $query->where('first_name','LIKE',"%$search%");
                // ->orWhere('last_name','LIKE',"%$search%")
                // ->orWhere('chapel_name','LIKE',"%$search%")
                // ->orWhere('attendance','LIKE',"%$search%");
            })->paginate(20);
        }
        else{
            $users = Participants::latest()->paginate(20);
        }
        return $users;
    }
    public function updateAtt($id){
        $array = explode(',', $id);
        foreach ($array as $idToUpdate){
            $participant = Participants::findOrFail($idToUpdate);
            $participant->update(['attendance'=>'1']);
        }
    }
    public function updatePay($id){
        $array = explode(',', $id);
        foreach ($array as $idToUpdate){
            $participant = Participants::findOrFail($idToUpdate);
            $participant->update(['payment'=>'1']);
            
            
        }
    }
    public function updateAttPay($id){
        $array = explode(',', $id);
        foreach ($array as $idToUpdate){
            $participant = Participants::findOrFail($idToUpdate);
            $participant->update(['attendance'=>'1','payment'=>'1']);
        }
    }
   
    public function countTotal(){
        $countPayment = Participants::where('payment','1')->count();
        $countRegistered = Participants::where('attendance','1')->count();
        $payments = array($countPayment,$countRegistered);
        return $payments;
    }

}
